package com.example.plugins

import com.example.routes.modeRouting
import com.example.routes.roomRouting
import com.example.routes.statusRouting
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Application.configureRouting() {
    routing {
        get("/") {
            call.respondText("Hello Robot Server!")
        }
        statusRouting()
        roomRouting()
        modeRouting()
    }
}
