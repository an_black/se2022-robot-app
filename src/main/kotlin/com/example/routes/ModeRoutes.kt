package com.example.routes

import com.example.models.initializeMode
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.modeRouting() {
    val modeStorage = initializeMode()
    route("/modes") {
        get {
            if (modeStorage.isNotEmpty()) {
                call.respond(modeStorage)
            } else {
                call.respondText("No modes found", status = HttpStatusCode.OK)
            }
        }

    }
}