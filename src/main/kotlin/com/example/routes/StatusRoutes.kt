package com.example.routes

import com.example.models.Status
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.statusRouting() {
    route("/status") {
        get {
            val connection = Status(true)
            if(connection.getStatus()) {
                call.respondText("Connected", status = HttpStatusCode.OK)
            } else {
                call.respondText("No connection", status = HttpStatusCode.OK)
            }
        }
    }
}