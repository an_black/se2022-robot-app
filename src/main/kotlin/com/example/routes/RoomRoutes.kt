package com.example.routes

import com.example.models.initializeRooms
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.roomRouting() {
    val roomStorage = initializeRooms()
    route("/rooms") {
        get {
            if (roomStorage.isNotEmpty()) {
                call.respond(roomStorage)
            } else {
                call.respondText("No rooms found", status = HttpStatusCode.OK)
            }
        }
    }
}