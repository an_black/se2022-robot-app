package com.example.models

open class Status(status: Boolean) {
    private val isConnected: Boolean = status

    fun getStatus(): Boolean {
        return isConnected
    }
}