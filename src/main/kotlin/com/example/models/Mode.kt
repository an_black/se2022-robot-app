package com.example.models

import com.example.factories.ModeFactory
import com.example.factories.ModeType
import kotlinx.serialization.Serializable

@Serializable
open class Mode(val speed: String, val name: String) {
}

fun initializeMode(): MutableList<Mode> {
    val modeStorage = mutableListOf<Mode>()
    val factory = ModeFactory()

    val modeDefault = factory.createMode(ModeType.DEFAULT)
    val modeLight = factory.createMode(ModeType.LIGHT)
    val modeIntense = factory.createMode(ModeType.INTENSE)

    modeStorage.add(modeDefault)
    modeStorage.add(modeLight)
    modeStorage.add(modeIntense)

    return modeStorage
}