package com.example.models

import com.example.factories.RoomFactory
import com.example.factories.RoomType
import kotlinx.serialization.*

@Serializable
open class Room(val name: String) {
    open fun createMap(): MapObject {
        return MapObject(name)
    }
    open fun startCleaning() {}
}

fun initializeRooms(): MutableList<Room> {
    val roomStorage = mutableListOf<Room>()
    val factory = RoomFactory()

    val room1 = factory.makeRoom(RoomType.DINING_ROOM)
    val room2 = factory.makeRoom(RoomType.LIVING_ROOM)
    val room3 = factory.makeRoom(RoomType.HALL)

    roomStorage.add(room1)
    roomStorage.add(room2)
    roomStorage.add(room3)
    return roomStorage
}
