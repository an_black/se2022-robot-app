package com.example.models

// for simplicity reason map class has only a name that correspond to a room it belongs to
class MapObject(name: String) {

    init {
        println("Map initialized with the following name: $name")
    }
}