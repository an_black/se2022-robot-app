package com.example.factories

import com.example.models.Mode

class DefaultMode: Mode("1", "Default") {
}

class LightMode: Mode("0.5", "Light") {
}

class IntenseMode: Mode("2", "Intense") {
}

enum class ModeType {
    DEFAULT,
    LIGHT,
    INTENSE
}

class ModeFactory {
    fun createMode(type: ModeType): Mode {
        return when(type) {
            ModeType.DEFAULT -> DefaultMode()
            ModeType.LIGHT -> LightMode()
            ModeType.INTENSE -> IntenseMode()
        }
    }
}