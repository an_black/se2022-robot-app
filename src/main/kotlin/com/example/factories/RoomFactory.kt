package com.example.factories

import com.example.models.MapObject
import com.example.models.Room

class DiningRoom: Room(name = "Dining Room") {

    override fun createMap(): MapObject {
        return MapObject("Dining Room Map")
    }

    override fun startCleaning() {
        println("DiningRoom: cleaning started")
    }
}

class LivingRoom: Room(name = "Living Room") {
    override fun createMap(): MapObject {
        return MapObject("Living Room Map")
    }

    override fun startCleaning() {
        println("LivingRoom: cleaning started")
    }
}

class Hall: Room(name = "Hall Room") {
    override fun createMap(): MapObject {
        return MapObject("Hall Room Map")
    }

    override fun startCleaning() {
        println("Hall: cleaning started")
    }
}

enum class RoomType {
    LIVING_ROOM,
    DINING_ROOM,
    HALL,
}

class RoomFactory {
    fun makeRoom(type: RoomType): Room {
        return when(type) {
            RoomType.DINING_ROOM -> DiningRoom()
            RoomType.LIVING_ROOM -> LivingRoom()
            RoomType.HALL -> Hall()
        }
    }
}