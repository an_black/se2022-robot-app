package com.example

import com.example.plugins.configureHTTP
import com.example.plugins.configureRouting
import com.example.plugins.configureSecurity
import com.example.plugins.configureSockets
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.testing.*
import org.junit.jupiter.api.Order
import kotlin.test.Test
import kotlin.test.assertEquals

class ApplicationTest {
    @Test
    @Order(1)
    fun testRoot() = testApplication {
        application {
            configureRouting()
        }
        client.get("/").apply {
            assertEquals(HttpStatusCode.OK, status)
            assertEquals("Hello Robot Server!", bodyAsText())
        }
    }

    @Test
    @Order(2)
    fun `returns all created rooms`() = testApplication {
        application {
            configureRouting()
            configureSockets()
            configureHTTP()
            configureSecurity()
        }

        val client = createClient {
            this@testApplication.install(ContentNegotiation) {
                json()
            }
        }

        val response = client.get("/rooms")

        assertEquals(
            """[{"name":"Dining Room"},{"name":"Living Room"},{"name":"Hall Room"}]""".trimIndent(),
            response.bodyAsText()
        )
        assertEquals(HttpStatusCode.OK, response.status)
    }

    @Test
    @Order(3)
    fun `returns all available modes`() = testApplication {
        application {
            configureRouting()
            configureSockets()
            configureHTTP()
            configureSecurity()
        }

        val client = createClient {
            this@testApplication.install(ContentNegotiation) {
                json()
            }
        }

        val response = client.get("/modes")

        assertEquals(
            """[{"speed":"1","name":"Default"},{"speed":"0.5","name":"Light"},{"speed":"2","name":"Intense"}]""".trimIndent(),
            response.bodyAsText()
        )
        assertEquals(HttpStatusCode.OK, response.status)
    }

    @Test
    @Order(4)
    fun `returns connection status`() = testApplication {
        application {
            configureRouting()
            configureSockets()
            configureHTTP()
            configureSecurity()
        }

        val client = createClient {
            this@testApplication.install(ContentNegotiation) {
                json()
            }
        }

        val response = client.get("/status")

        assertEquals(
            """Connected""".trimIndent(),
            response.bodyAsText()
        )
        assertEquals(HttpStatusCode.OK, response.status)
    }
}