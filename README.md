# SE2022-robot-app



## Intoduction

This is the project for the laboratory tasks demostration in the course Software Engineering 2022.

## Getting started

The project implements a simple HTTP server that exposes API for fetching data from the local memory storage. The idea is to apply commom design patterns and support client with mock information. The factory design patter was used to create a list of Rooms and cleaning modes.

## Run the project
To run the project:
`./gradle run`

To Test the project
`./gradlew test`

To see the list of all available gradle commands:
`./gradlew tasks`